<?php
	class view_controller extends controller {
		private function list_games($name, $status, $user_id) {
			if (($count = $this->model->count_games($status, $user_id)) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$paging = new pagination($this->output, $name, 10, $count);

			if (($games = $this->model->get_games($status, $paging->offset, $paging->size, $user_id)) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			if ($status == GAME_ACTIVE) {
				$section = "Active";
			} else if ($this->page->pathinfo[2] == "my") {
				$section = "My finished";
			} else {
				$section = "Finished";
			}

			$this->output->open_tag("overview");

			$this->output->open_tag("games", array("section" => $section, "order" => $status == GAME_ACTIVE ? "Start date" : "Last move"));
			foreach ($games as $game) {
				$game["start"] = date("j F Y", $game["start"]);
				$this->output->record($game, "game");
			}
			$this->output->close_tag();

			$paging->show_browse_links();

			$this->output->close_tag();
		}

		private function view_game($game_id) {
			/* View game
			 */
			if (valid_input($game_id, VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$moves = null;
			} else {
				$moves = $game_id;
			}

			$chess = new chess_game($this->db, $this->page->pathinfo[1]);
			if ($chess->error) {
				$this->output->add_tag("result", "Error loading chess game.");
				return;
			}

			$args = array(
				"id"     => $chess->game_id,
				"link"   => $chess->info["status"] == GAME_ACTIVE ? "" : "/finished",
				"status" => $chess->info["status"]);
			if ($moves !== null) {
				$args["moves"] = $moves;
			}

			$this->output->open_tag("game", $args);

			$chess->make_moves($moves);

			if ($chess->info["status"] == GAME_DONE) {
				if ($chess->info["surrender"] != 0) {
					$user = $this->db->entry("users", $chess->info["surrender"]);
					$this->output->add_tag("surrendered", $user["fullname"]);
				} else if ($chess->info["draw"] != 0) {
					$this->output->add_tag("draw", "yes");
				}
			}
			$this->output->add_tag("white", $chess->white["fullname"], array("id" => $chess->white["id"]));
			$this->output->add_tag("black", $chess->black["fullname"], array("id" => $chess->black["id"]));

			$chess->draw_board($this->output, $this->settings->board_texture);

			$chess->make_moves();
			$chess->show_moves($this->output, $moves);

			/* Comments
			 */
			$comments = new comments($this->db, $chess->game_id);
			if (($chess->info["status"] == GAME_DONE) && ($_SERVER["REQUEST_METHOD"] == "POST")) {
				if (($comment = trim($_POST["comment"])) != "") {
					$comments->add($_POST["comment"], $this->user->id);
				}
			}
			$comments->to_output($this->output);

			$this->output->close_tag();
		}

		public function execute() {
			if (valid_input($this->page->pathinfo[1], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				$this->view_game($this->page->pathinfo[2]);
			} else {
				/* View games
				 */
				$name = "view";
				if ($this->page->pathinfo[1] == "finished") {
					$name = "finished_".$name;
					$status = GAME_DONE;
				} else {
					$status = GAME_ACTIVE;
				}
				if ($this->page->pathinfo[2] == "my") {
					$name = "my_".$name;
					$user_id = $this->user->id;
				} else {
					$user_id = null;
				}

				$this->list_games($name, $status, $user_id);
			}
		}
	}
?>
