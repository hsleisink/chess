<?php
	class statistics_controller extends controller {
		public function execute() {
			if (($stats = $this->model->get_statistics()) == false) {
				$this->output->add_tag("result", "Database error");
				return;
			}

			$this->output->add_javascript("jquery.tablesorter.js");
			$this->output->add_javascript("statistics.js");

			$this->output->open_tag("statistics");

			foreach ($stats as $stat) {
				$played = (int)$stat["games_total"] - (int)$stat["games_active"];
				$won = (int)$stat["games_won"];
				$draw = (int)$stat["games_draw"];
				if ($played > 0) {
					$stat["rate"] = round(100 * ((0.5 * $draw + $won) / $played));
				} else {
					$stat["rate"] = 0;
				}
				$stat["score"] = sprintf("%0.1f", 0.5 * $draw + $won);
				$this->output->record($stat, "user");
			}

			$this->output->close_tag();
		}
	}
?>
