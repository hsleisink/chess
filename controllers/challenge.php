<?php
	class challenge_controller extends controller {
		private $url = null;

		private function show_challenge_form() {
			if (($users = $this->model->get_available_users()) === false) {
				$this->output->add_tag("result", "Error getting user list.", $this->url);
				return;
			}

			if (($games = $this->model->get_pending_games()) === false) {
				$this->output->add_tag("result", "Error getting pending games.", $this->url);
				return;
			}

			$this->output->open_tag("overview");

			$this->output->open_tag("users");
			foreach ($users as $user) {
				$time = time() - (int)$user["last_action"];
				if ($time > $this->settings->session_timeout) {
					$user["online"] = "no";
				} else if ($time < 60) {
					$user["online"] = $time." seconds";
				} else if ($time < 3600) {
					$user["online"] = sprintf("%d:%02d minutes", $time / 60, $time % 60);
				}
				$user["last_color"] = $this->model->last_color($user["id"]);
				$this->output->record($user, "user");
			}
			$this->output->close_tag();

			$this->output->open_tag("pending");
			foreach ($games as $game) {
				$this->output->record($game, "game");
			}
			$this->output->close_tag();

			$this->output->close_tag();
		}

		public function execute() {
			$this->url = array("url" => "challenge");

			if ($_SERVER["REQUEST_METHOD"] == "POST") { 
				if ($_POST["submit_button"] == "Challenge") {
					if ($this->model->game_oke($_POST) == false) {
						$this->show_challenge_form();
					} else if ($this->model->create_game($_POST) == false) {
						$this->output->add_tag("result", "Error while creating game.", $this->url);
					} else {
						$this->model->send_challenge_notification($this->db->last_insert_id());
						$this->output->add_tag("result", "Game has been created.", $this->url);
					}
				} else if ($_POST["submit_button"] == "Accept") {
					if ($this->model->accept_challenge($_POST["game_id"]) == false) {
						$this->output->add_tag("result", "Error while accepting the game.", $this->url);
					} else {
						$this->model->send_accept_notification($_POST["game_id"]);
						$this->output->add_tag("result", "You've accepted the challenge.", array("url" => "game/".$_POST["game_id"]));
					}
				} else if ($_POST["submit_button"] == "Reject") {
					$this->model->send_reject_notification($_POST["game_id"]);
					if ($this->model->reject_challenge($_POST["game_id"]) == false) {
						$this->output->add_tag("result", "Error while rejecting the game.", $this->url);
					} else {
						$this->output->add_tag("result", "You've rejected the challenge.", $this->url);
					}
				} else {
					$this->show_challenge_form();
				}
			} else {
				$this->show_challenge_form();
			}
		}
	}
?>
