<?php
	class cms_test_controller extends controller {
		private function show_chessboard($chess) {
			$this->output->open_tag("game", array(
				"id"          => $chess->info["id"],
				"my_turn"     => "yes",
				"type"        => "email",
				"draw_offer"  => show_boolean($chess->draw_offer),
				"surrendered" => show_boolean($chess->surrendered),
				"check"       => show_boolean($chess->check)));

			$this->output->add_tag("white", $chess->white["fullname"], array("id" => $chess->white["id"]));
			$this->output->add_tag("black", $chess->black["fullname"], array("id" => $chess->black["id"]));
			$this->output->add_tag("my_color", $chess->move_count & 1);
			$chess->show_moves($this->output);
			$chess->draw_board($this->output, $this->settings->board_texture, false);
			$this->output->close_tag();
		}

		public function execute() {
			$chess = new chess_game_test($this->db);

			if ($chess->error || ($chess->my_color === null)) {
				$this->output->add_tag("result", "Error loading chess game.");
				return;
			}

			$chess->make_moves();

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($_POST["submit_button"] == "Move") {
					/* New move
					 */
					if ($chess->valid_move($_POST["from"], $_POST["to"], $_POST["promotion"]) == false) {
						$this->output->add_message("Invalid move!");
					} else if ($chess->new_move($_POST["from"], $_POST["to"], $_POST["promotion"]) == false) {
						$this->output->add_message("Database error!");
					}
				} else if ($_POST["submit_button"] == "Rollback") {
					/* Rollback move
					 */
					$chess->rollback();
				} else if ($_POST["submit_button"] == "Reset game") {
					/* Reset game
					 */
					$chess->reset_game();
				} else {
					$this->output->add_message("Huh?");
				}
			} else if ($chess->surrendered) {
				/* Opponent surrendered
				 */
				if ($chess->end_game() == false) {
					$this->output->add_message("Error ending game.");
				}
			}

			if ($chess->info["status"] == GAME_DONE) {
				$this->output->add_tag("result", "This game is over.");
			} else {
				$this->show_chessboard($chess);
			}
		}
	}
?>
