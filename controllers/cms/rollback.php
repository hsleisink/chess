<?php
	class cms_rollback_controller extends controller {
		public function execute() {
			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if ($this->model->rollback_move($_POST["game_id"]) == false) {
					$this->output->add_message("Error while rolling back move.");
				} else {
					$this->output->add_message("Move rolled back.");
				}
			}

			if (($games = $this->model->get_games()) === false) {
				$this->output->add_tag("result", "Database error.");
				return;
			}

			$this->output->open_tag("games");
			foreach ($games as $game) {
				$game["start"] = date("j F Y", $game["start"]);
				$game["turn"] = ($game["moves"] % 2 == 0) ? $game["white"] : $game["black"];
				$this->output->record($game, "game");
			}
			$this->output->close_tag();
		}
	}
?>
