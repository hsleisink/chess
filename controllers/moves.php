<?php
	class moves_controller extends controller {
		public function execute() {
			if (valid_input($this->page->pathinfo[1], VALIDATE_NUMBERS, VALIDATE_NONEMPTY) == false) {
				$this->output->add_tag("result", "Game not found.", array("url" => "menu"));
				return;
			}

			if (($game = $this->model->get_game($this->page->pathinfo[1])) === false) {
				$this->output->add_tag("result", "Game not found.", array("url" => "menu"));
				return;
			}

			if (($moves = $this->model->get_moves($this->page->pathinfo[1])) === false) {
				$this->output->add_tag("result", "Game not found.", array("url" => "menu"));
				return;
			}

			$this->output->open_tag("game", array("id" => $this->page->pathinfo[1]));

			$this->output->record($game);

			$promotion = array("", "Q", "B", "N", "R");

			$this->output->open_tag("moves");
			$this->output->open_tag("round");
			$count = 0;
			foreach ($moves as $move) {
				$move["to"] .= $promotion[$move["promotion"]];
				$this->output->record($move, "move");
				$count++;
				if (($count > 0) && ($count % 2 == 0)) {
					$this->output->close_tag();
					$this->output->open_tag("round");
				}
			}
			$this->output->close_tag();
			$this->output->close_tag();

			$this->output->close_tag();
		}
	}
?>
