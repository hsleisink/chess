<?php
	class game_controller extends controller {
		private function show_chessboard($chess) {
			$this->output->open_tag("game", array(
				"id"          => $chess->info["id"],
				"my_turn"     => show_boolean($chess->my_turn),
				"draw_offer"  => show_boolean($chess->draw_offer),
				"surrendered" => show_boolean($chess->surrendered),
				"check"       => show_boolean($chess->check)));

			$this->output->add_tag("white", $chess->white["fullname"], array("id" => $chess->white["id"]));
			$this->output->add_tag("black", $chess->black["fullname"], array("id" => $chess->black["id"]));
			$this->output->add_tag("my_color", $chess->my_color);

			$chess->show_moves($this->output);
			$chess->draw_board($this->output, $this->settings->board_texture, $this->user->id == $chess->black["id"]);

			$comments = new comments($this->db, $chess->game_id);
			$comments->to_output($this->output);

			$this->output->close_tag();
		}

		public function execute() {
			if (valid_input($this->page->pathinfo[1], VALIDATE_NUMBERS, VALIDATE_NONEMPTY)) {
				$chess = new chess_game($this->db, $this->page->pathinfo[1], $this->user->id);
				if ($chess->error || ($chess->my_color === null)) {
					$this->output->add_tag("result", "Error loading chess game.");
				} else {
					$chess->make_moves();

					if ($_SERVER["REQUEST_METHOD"] == "POST") {
						if ($chess->my_turn) {
							switch ($_POST["submit_button"]) {
								case "Move":
									/* New move
									 */
									if ($chess->valid_move($_POST["from"], $_POST["to"], $_POST["promotion"]) == false) {
										$this->output->add_message("Invalid move!");
									} else if ($chess->new_move($_POST["from"], $_POST["to"], $_POST["promotion"]) == false) {
										$this->output->add_message("Database error!");
									} else if (is_true($chess->opponent["notify"])) {
										$this->model->send_move_notification($chess);
									}
									break;
								case "Resign":
									/* Resign 
									 */
									if ($chess->surrender() == false) {
										$this->output->add_message("Error while surrendering.");
									} else if (is_true($chess->opponent["notify"])) {
										$this->model->send_surrender_notification($chess);
									}
									break;
								case "Offer draw":
									/* Offer draw
									 */
									if ($chess->offer_draw() == false) {
										$this->output->add_message("Error registering draw offer.");
									} else if (is_true($chess->opponent["notify"])) {
										$this->model->send_draw_notification($chess);
									}
									break;
								case "Accept":
									/* Accept draw offer
									 */
									if ($chess->end_game() == false) {
										$this->output->add_message("Error accepting draw offer.");
									} else if (is_true($chess->opponent["notify"])) {
										$this->model->send_accept_draw_notification($chess);
									}
									break;
								case "Reject":
									/* Reject draw offer
									 */
									if ($chess->reject_draw_offer() == false) {
										$this->output->add_message("Error rejecting draw offer.");
									} else if (is_true($chess->opponent["notify"])) {
										$this->model->send_reject_draw_notification($chess);
									}
									break;
							}
						}

						if ($_POST["submit_button"] == "Add comment") {
							/* Add comment
							 */
							if (($comment = trim($_POST["comment"])) != "") {
								$comments = new comments($this->db, $chess->game_id);
								$comments->add($comment, $this->user->id);
							}
						}
					} else if ($chess->surrendered) {
						/* Opponent surrendered
						 */
						if ($chess->end_game() == false) {
							$this->output->add_message("Error ending game.");
						}
					}

					if ($chess->info["status"] == GAME_DONE) {
						$this->output->add_tag("result", "This game is over.");
					} else {
						$this->show_chessboard($chess);
					}
				}
			} else {
				/* Show games
				 */
				if (($games = $this->model->get_games()) === false) {
					$this->output->add_tag("result", "Database error");
					return;
				}

				/* Theme
				 */
				list($day, $month, $year) = explode("-", date("j-n-Y"));

				$theme = "";
				if ($month == 1) {
					if ($day < 6) {
						#$theme = "_nieuwjaar";
					}
				} else if (($month == 4) && ($year == 2017)) {
					if (($day == 16) || ($day == 17)) {
						$theme = "_pasen";
					}
				} else if (($month == 4) && ($year == 2018)) {
					if (($day == 1) || ($day == 2)) {
						$theme = "_pasen";
					}
				} else if ($month == 11) {
					if ($day > 20) {
						$theme = "_sint";
					}
				} else if ($month == 12) {
					if ($day < 6) {
						$theme = "_sint";
					} else if (($day > 9) && ($day < 27)) {
						$theme = "_kerst";
					}
				}

				$this->output->open_tag("games", array("theme" => $theme));
				foreach ($games as $game) {
					$game["start"] = date("j F Y", $game["start"]);
					$game["opponent"] = $game["white"] == $this->user->id ? $game["name_black"] : $game["name_white"];

					if (($game["draw"] == $this->user->id) || ($game["surrender"] == $this->user->id)) {
						$my_turn = false;
					} else if (($game["draw"] != 0) || ($game["surrender"] != 0)) {
						$my_turn = true;
					} else {
						$my_turn = (($game["black"] == $this->user->id) xor ($game["moves"] % 2 == 0));
					}
					$game["my_turn"] = show_boolean($my_turn);

					$this->output->record($game, "game");
				}
				$this->output->close_tag();
			}
		}
	}
?>
