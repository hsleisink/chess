<?php
	include("../libraries/comments.php");

	class game_model extends model {
		public function get_games() {
			$query = "select *, UNIX_TIMESTAMP(start) as start, ".
					 "(select fullname from users where id=g.white) as name_white, ".
					 "(select fullname from users where id=g.black) as name_black, ".
					 "(select count(*) from moves where game_id=g.id) as moves ".
					 "from games g where status=%d and (white=%d or black=%d) ".
					 "order by start desc";

			return $this->db->execute($query, GAME_ACTIVE, $this->user->id, $this->user->id);
		}

/*
		public function count_moves($game_id) {
			$query = "select count(*) as count from moves where game_id=%d";

			if (($result = $this->db->execute($query, $game_id)) == false) {
				return false;
			}

			return $result[0]["count"];
		}
*/

		public function send_move_notification($chess) {
			$email = new chess_email($this->db, "Opponent made a move");
			$email->set_link("/game/".$chess->info["id"], $chess->opponent["id"]);
			$email->message("Your opponent, ".$chess->player["fullname"].", has made a move.");
			$email->send($chess->opponent["email"], $chess->opponent["fullname"]);
		}

		public function send_draw_notification($chess) {
			$email = new chess_email($this->db, "Opponent offered a draw");
			$email->set_link("/game/".$chess->info["id"], $chess->opponent["id"]);
			$email->message("Your opponent, ".$chess->player["fullname"].", has offered you a draw.");
			$email->send($chess->opponent["email"], $chess->opponent["fullname"]);
		}
		
		public function send_accept_draw_notification($chess) {
			$email = new chess_email($this->db, "Opponent accepted draw");
			$email->set_link("/game/".$chess->info["id"], $chess->opponent["id"]);
			$email->message("Your opponent, ".$chess->player["fullname"].", has accepted your draw offer.");
			$email->send($chess->opponent["email"], $chess->opponent["fullname"]);
		}

		public function send_reject_draw_notification($chess) {
			$email = new chess_email($this->db, "Opponent rejected draw");
			$email->set_link("/game/".$chess->info["id"], $chess->opponent["id"]);
			$email->message("Your opponent, ".$chess->player["fullname"].", has rejected your draw offer.");
			$email->send($chess->opponent["email"], $chess->opponent["fullname"]);
		}

		public function send_surrender_notification($chess) {
			$email = new chess_email($this->db, "Opponent has resigned");
			$email->set_link("/game/".$chess->info["id"], $chess->opponent["id"]);
			$email->message("Your opponent, ".$chess->player["fullname"].", has resigned.");
			$email->send($chess->opponent["email"], $chess->opponent["fullname"]);
		}
	}
?>
