<?php
	class statistics_model extends model {
		function get_statistics() {
			$query = "select *, ".
					 "(select count(*) from games where status=%d and (white=u.id or black=u.id) and surrender!=u.id and surrender!=0) as games_won, ".
					 "(select count(*) from games where status=%d and surrender=u.id) as games_lost, ".
					 "(select count(*) from games where status=%d and (white=u.id or black=u.id) and draw!=0) as games_draw, ".
					 "(select count(*) from games where status=%d and (white=u.id or black=u.id)) as games_active, ".
					 "(select count(*) from games where status>%d and (white=u.id or black=u.id)) as games_total ".
					 "from users u where id>0 and status>=%d order by fullname";

			return $this->db->execute($query, GAME_DONE, GAME_DONE, GAME_DONE, GAME_ACTIVE, GAME_PENDING, USER_STATUS_DISABLED);
		}
	}
?>
