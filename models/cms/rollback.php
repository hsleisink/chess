<?php
	class cms_rollback_model extends model {
		public function get_games() {
			$query  = "select *, UNIX_TIMESTAMP(start) as start, ".
					  "(select fullname from users where id=g.white) as white, ".
					  "(select fullname from users where id=g.black) as black, ".
					  "(select count(*) from moves where game_id=g.id) as moves ".
					  "from games g where status=%d order by start desc";

			return $this->db->execute($query, GAME_ACTIVE);
		}

		public function rollback_move($game_id) {
			$query = "select id from moves where game_id=%d order by date desc limit 1";
			if (($result = $this->db->execute($query, $game_id)) == false) {
				return false;
			}
			$move_id = $result[0]["id"];

			return $this->db->delete("moves", $move_id) !== false;
		}
	}
?>
