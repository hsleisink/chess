<?php
	class moves_model extends model {
		public function get_game($game_id) {
			$query = "select status, ".
					 "(select fullname from users where white=id) as white, ".
					 "(select fullname from users where black=id) as black ".
					 "from games g where id=%d.";
			if (($result = $this->db->execute($query, $game_id)) === false) {
				return false;
			}

			return $result[0];
		}

		public function get_moves($game_id) {
			$query = "select * from moves where game_id=%d";

			return $this->db->execute($query, $game_id);
		}
	}
?>
