<?php
	class challenge_model extends model {
		public function last_color($opponent_id) {
			$query = "select * from games where (white=%d and black=%d) or (white=%d and black=%d) order by start desc limit 1";
			if (($result = $this->db->execute($query, $this->user->id, $opponent_id, $opponent_id, $this->user->id)) === false) {
				return "";
			} else if (count($result) == 0) {
				return "-";
			}
			$game = $result[0];

			return $game["white"] == $this->user->id ? "white" : "black";
		}

		public function get_available_users() {
			$query = "select u.*, UNIX_TIMESTAMP(last_action) as last_action ".
				"from users u, user_role m where ".
				"u.id=m.user_id and m.role_id=%d and u.id!=%d and u.id not in (".
				"select white as id from games where status<=%d and black=%d ".
				"union ".
				"select black as id from games where status<=%d and white=%d ".
				") and (u.status=%d or u.status=%d) order by u.fullname";

			return $this->db->execute($query, USER_ROLE_ID, $this->user->id, GAME_ACTIVE, $this->user->id, GAME_ACTIVE, $this->user->id, USER_STATUS_ACTIVE, USER_STATUS_CHANGEPWD);
		}

		public function get_pending_games() {
			$query = "select * from games g where status=%d and (white=%d or black=%d) order by start";

			if (($games = $this->db->execute($query, GAME_PENDING, $this->user->id, $this->user->id, $this->user->id)) === false) {
				return false;
			}

			foreach ($games as $index => $game) {
				if ($this->user->id == $game["white"]) {
					$opponent = $this->db->entry("users", $game["black"]);
				} else {
					$opponent = $this->db->entry("users", $game["white"]);
				}

				$games[$index]["opponent"] = $opponent["fullname"];
			}

			return $games;
		}

		public function game_oke($game) { 
			$result = true;

			if (isset($game["opponent_id"]) == false) {
				$this->output->add_message("Select an opponent first.");
				$result = false;
			} else if ($this->db->entry("users", $game["opponent_id"]) == false) {
				$this->output->add_message("Select an opponent first.");
				$result = false;
			}

			if (($game["my_color"] != 0) && ($game["my_color"] != 1)) {
				$this->output->add_message("Invalid color.");
				$result = false;
			}

			return $result;
		}

		public function create_game($game) {
			if ($game["my_color"] == WHITE) {
				$white_id = $this->user->id;
				$black_id = $game["opponent_id"];
			} else if ($game["my_color"] == BLACK) {
				$white_id = $game["opponent_id"];
				$black_id = $this->user->id;
			} else {
				return false;
			}

			$values = array(
				"id" => null,
				"creator"   => $this->user->id,
				"white"     => $white_id,
				"black"     => $black_id,
				"start"     => null,
				"status"    => GAME_PENDING,
				"surrender" => 0,
				"draw"      => 0);

			return $this->db->insert("games", $values) !== false;
		}

		public function accept_challenge($game_id) {
			$query = "update games set status=%d where id=%d and status=%d and creator!=%d and (white=%d or black=%d)";

			return $this->db->query($query, GAME_ACTIVE, $game_id, GAME_PENDING, $this->user->id, $this->user->id, $this->user->id) !== false;
		}

		public function reject_challenge($game_id) {
			$query = "delete from games where id=%d and status=%d and creator!=%d and (white=%d or black=%d)";

			return $this->db->query($query, $game_id, GAME_PENDING, $this->user->id, $this->user->id, $this->user->id) !== false;
		}

		private function get_mail_info($game_id) {
			if (($game = $this->db->entry("games", $game_id)) == false) {
				return false;
			}

			if ($this->user->id == $game["white"]) {
				$game["player"]   = $this->db->entry("users", $game["white"]);
				$game["opponent"] = $this->db->entry("users", $game["black"]);
			} else if ($this->user->id == $game["black"]) {
				$game["player"]   = $this->db->entry("users", $game["black"]);
				$game["opponent"] = $this->db->entry("users", $game["white"]);
			} else {
				return false;
			}

			return $game;
		}

		public function send_challenge_notification($game_id) {
			if (($game = $this->get_mail_info($game_id)) != false) {
				$color = ($this->user->id == $game["white"]) ? "black" : "white";

				$email = new chess_email($this->db, "You have been challenged");
				$email->set_link("/challenge", $game["opponent"]["id"]);
				$email->message("You have been challenged by ".$game["player"]["fullname"]." for a game of chess. You will play with ".$color.".");
				$email->send($game["opponent"]["email"], $game["opponent"]["fullname"]);
			}
		}

		public function send_accept_notification($game_id) {
			if (($game = $this->get_mail_info($game_id)) != false) {
				$email = new chess_email($this->db, "Challenge accepted");
				$email->set_link("/game/".$game_id, $game["opponent"]["id"]);
				$email->message($game["player"]["fullname"]." has accepted your chess challenge.");
				$email->send($game["opponent"]["email"], $game["opponent"]["fullname"]);
			}
		}

		public function send_reject_notification($game_id) {
			if (($game = $this->get_mail_info($game_id)) != false) {
				$email = new chess_email($this->db, "Challenge rejected");
				$email->set_link("/game/".$game_id, $game["opponent"]["id"]);
				$email->message($game["player"]["fullname"]." has rejected your chess challenge.");
				$email->send($game["opponent"]["email"], $game["opponent"]["fullname"]);
			}
		}
	}
?>
