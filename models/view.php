<?php
	class view_model extends model {
		public function count_games($status, $user_id) {
			$query = "select count(*) as count from games where status=%d";
			$args = array($status);

			if ($user_id !== null) {
				$query .= " and (white=%d or black=%d) ";
				array_push($args, $user_id, $user_id);
			}

			if (($result = $this->db->execute($query, $args)) == false) {
				return false;
			}

			return $result[0]["count"];
		}

		public function get_games($status, $offset, $limit, $user_id = null) {
			$query  = "select *, UNIX_TIMESTAMP(start) as start, ".
					  "(select fullname from users where id=g.white) as white, ".
					  "(select fullname from users where id=g.black) as black, ".
					  "(select count(*) from comments where game_id=g.id) as comments, ".
					  "(select date from moves where game_id=g.id order by date desc limit 1) as last_move ".
					  "from games g where status=%d";
			$args = array($status);

			if ($user_id !== null) {
				$query .= " and (g.white=%d or g.black=%d)";
				array_push($args, $user_id, $user_id);
			}

			$query .= " order by ".($status == GAME_ACTIVE ? "start" : "last_move")." desc limit %d,%d";
			array_push($args, $offset, $limit);

			return $this->db->execute($query, $args);
		}
	}
?>
