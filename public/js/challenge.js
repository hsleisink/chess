ajax = new ajax();

function update_previous_color(result) {
	color = result.getValue("color");

	ajax_clear("previous_game");
	if (color == "null") {
		ajax_print("previous_game", "You have never played against this opponent before.");
	} else if ((color == "white") || (color == "black")) {
		ajax_print("previous_game", "You played with <b>" + color + "</b> in your last game with this opponent.");
	}
}

function get_previous_color(opponent) {
	ajax.get("challenge", "opponent=" + opponent, update_previous_color)
}
