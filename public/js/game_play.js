var stage = 0;
var first = null;

function cell_hover(cell) {
	if (stage < 2) {
		field = stage == 0 ? "from" : "to";

		input = document.getElementById(field);
		input.value = cell;
	}
}
function cell_leave() {
	if (stage < 2) {
		field = stage == 0 ? "from" : "to";

		input = document.getElementById(field);
		input.value = "";
	}
}

function cell_clicked(cell) {
	field = stage == 0 ? "from" : "to";

	input = document.getElementById(field);
	cell_tag = document.getElementById("cell_" + cell);

	if ((stage > 0) && (cell_tag.value != 0)) {
		first_tag = document.getElementById("cell_" + first);
		reset = false;

		if ((my_color == 0) && (cell_tag.value < 10) && (first_tag.value < 10)) {
			reset = true;
		}
		if ((my_color == 1) && (cell_tag.value >= 10) && (first_tag.value >= 10)) {
			reset = true;
		}

		if (reset) {
			move_reset();
			cell_hover(cell);
			cell_clicked(cell);
			return;
		}
	}

	switch (stage) {
		case 0:
			if (cell_tag.value == 0) {
				return;
			}

			if ((my_color == 1) && (cell_tag.value < 10)) {
				return;
			}
			if ((my_color == 0) && (cell_tag.value >= 10)) {
				return;
			}

			input.style.color = "#0000ff";
			stage = 1;
			cell_hover(cell);
			place_marker(cell, "marker1");
			first = cell;
			break;
		case 1:
			input.style.color = "#0000ff";
			stage = 2;
			place_marker(cell, "marker2");
			break;
		case 2:
			input.value = cell;
			place_marker(cell, "marker2");
			break;
	}


	/* Promotion
	 */
	if (stage == 2) {
		from = document.getElementById("from");
		cell_tag = document.getElementById("cell_" + from.value);
		div = document.getElementById("promotion");

		if ((cell_tag.value == 1) && (input.value[1] == 8)) {
			div.style.display = "block";
		} else if ((cell_tag.value == 11) && (input.value[1] == 1)) {
			div.style.display = "block";
		} else {
			div.style.display = "none";
		}
	}
}

function place_marker(cell, marker) {
	div = document.getElementById(marker);

	if (board_flipped == "yes") {
		x = (104 - cell.charCodeAt(0)) * 62 + 19;
		y = (cell.charCodeAt(1) - 49) * 60 + 4;
	} else {
		x = (cell.charCodeAt(0) - 97) * 62 + 19;
		y = (56 - cell.charCodeAt(1)) * 60 + 4;
	}

	div.style.left = x + "px";
	div.style.top = y + "px";
	div.style.display = "block";
}

function move_reset() {	
	stage = 0;
	first = null;

	input = document.getElementById("from");
	input.style.color = "#ff2020";
	input.value = "";

	input = document.getElementById("to");
	input.style.color = "#ff2020";
	input.value = "";

	div = document.getElementById("marker1");
	div.style.display = "none";

	div = document.getElementById("marker2");
	div.style.display = "none";

	div = document.getElementById("promotion");
	div.style.display = "none";

	if ((div = document.getElementById("warnings")) != null) {
		div.style.display = "none";
	}
}
