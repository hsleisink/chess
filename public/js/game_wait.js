var ajax_turn = new ajax();

function move_check() {
	ajax_turn.get("game", "check_turn=" + game_id, handle_turn);
	setTimeout(move_check, 10000);
}

function handle_turn(result) {
	turn = result.getValue("turn");

	if ((turn === "true") || ((turn !== "false") && (turn == my_color))) {
		document.location = '/game/' + game_id;
	}
}

move_check();
