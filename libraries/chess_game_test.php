<?php
	class chess_game_test extends chess_game {
		public function __construct($db) {
			$this->board = $this->default_board;
			$this->db = $db;
			$this->game_id = 0;

			$query = "select * from users order by id limit 2";
			if (($users = $db->execute($query)) === false) {
				return false;
			} else if (count($users) < 2) {
				return false;
			}

			$this->info = array(
				"id"        => $this->game_id,
				"creator"   => $users[0]["id"],
				"white"     => $users[0]["id"],
				"black"     => $users[1]["id"],
				"start"     => "2013-06-13 01:04:00",
				"status"    => 1,
				"surrender" => 0,
				"draw"      => 0);

			if (isset($_SESSION["test_moves"]) == false) {
				$_SESSION["test_moves"] = array();
			}
			$this->moves = $_SESSION["test_moves"];

			$this->white = $db->entry("users", $this->info["white"]);
			$this->black = $db->entry("users", $this->info["black"]);

			$user_id = (count($this->moves) % 2) + 1;
			if ($user_id == $this->white["id"]) {
				$this->player = &$this->white;
				$this->opponent = &$this->black;
				$this->my_color = WHITE;
			} else if ($user_id == $this->black["id"]) {
				$this->player = &$this->black;
				$this->opponent = &$this->white;
				$this->my_color = BLACK;
			}

			if ($this->my_color !== null) {
				$this->my_turn = $this->my_color == WHITE;
			}
		}

		public function __destruct() {
			$_SESSION["test_moves"] = $this->moves;
		}

		public function new_move($from, $to, $promotion) {
			if ($this->my_turn == false) {
				return false;
			}

			list($to_x, $to_y) = $this->position_to_coordinate($to);
			$hit = $this->board[$to_y][$to_x] != NOTHING;
			$this->make_move($from, $to, $promotion);
			$piece = $this->board[$to_y][$to_x];

			array_push($this->moves, array(
				"id"        => null,
				"game_id"   => $this->game_id,
				"from"      => $from,
				"to"        => $to,
				"date"      => time(),
				"promotion" => $promotion,
				"piece"     => $piece,
				"hit"       => $hit));

			$this->my_color = 1 - $this->my_color;
			$this->check = $this->king_under_attack();
			$this->my_color = 1 - $this->my_color;

			$this->my_turn = true;

			return true;
		}

		public function rollback() {
		 	array_pop($this->moves);
			$this->move_count = 0;
			$this->board = $this->default_board;
			$this->make_moves();

			$this->my_turn = true;
		}

		public function reset_game() {
			$this->moves = array();
			$this->move_count = 0;
			$this->board = $this->default_board;
			$this->check = false;
		}
	}
?>
