<?php
	/* libraries/aes256.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * http://www.banshee-php.org/
	 */

	class AES256 {
		private $mode = "aes-256-";
		private $crypto_key = null;
		private $iv = null;

		/* Constructor
		 *
		 * INPUT:  string crypto key[, string iv]
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($crypto_key, $iv = null, $mode = "cbc") {
			if (strlen($crypto_key) < 32) {
				$crypto_key .= hash("sha256", $crypto_key);
			}

			$this->crypto_key = substr($crypto_key, 0, 32);
			$this->mode .= $mode;

			$iv_size = openssl_cipher_iv_length($this->mode);
			if ($iv === null) {
				$iv = openssl_random_pseudo_bytes($iv_size);
			} else while (strlen($iv) < $iv_size) {
				$iv .= $iv;
			}
			$this->iv = mb_substr($iv, 0, $iv_size, "8bit");
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "mode": return $this->mode;
				case "iv": return $this->iv;
			}

			return null;
		}

		/* Encrypt data
		 *
		 * INPUT:  string plain text data, [bool encode URL-friendly]
		 * OUTPUT: string encrypted data
		 * ERROR:  false
		 */
		public function encrypt($data, $encode = false) {
			if ($this->crypto_key == null) {
				return false;
			}

			$data = openssl_encrypt($data, $this->mode, $this->crypto_key, OPENSSL_RAW_DATA, $this->iv);

			if (($data !== false) && $encode) {	
				$data = base64_encode($data);
				$data = strtr($data, "/+=", "_-:");
			}

			return $data;
		}

		/* Decrypt data
		 *
		 * INPUT:  string encrypted data, [bool URL-friendly encoded]
		 * OUTPUT: string plain text data
		 * ERROR:  false
		 */
		public function decrypt($data, $encoded = false) {
			if ($this->crypto_key == null) {
				return false;
			}

			if ($encoded) {
				$data = strtr($data, "_-:", "/+=");
				$data = base64_decode($data);
			}

			return openssl_decrypt($data, $this->mode, $this->crypto_key, OPENSSL_RAW_DATA, $this->iv);
		}
	}
?>
