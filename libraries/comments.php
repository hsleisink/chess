<?php
	class comments {
		public function __construct($db, $game_id) {
			$this->db = $db;
			$this->game_id = $game_id;
		}

		public function add($comment, $user_id) {
			global $_user;

			$values = array(
				"id"      => null,
				"game_id" => $this->game_id,
				"user_id" => $user_id,
				"comment" => trim($comment),
				"date"    => null);

			return $this->db->insert("comments", $values) != false;
		}

		public function to_output($output) {
			$query = "select c.*,UNIX_TIMESTAMP(date) as date, u.fullname as author ".
					 "from comments c, users u ".
					 "where c.user_id=u.id and game_id=%d order by date";
			if (($comments =  $this->db->execute($query, $this->game_id)) === false) {
				return false;
			}

			$output->add_css("includes/comments.css");

			$output->open_tag("comments");

			foreach ($comments as $comment) {
				$comment["date"] = date("j F Y, H:i", $comment["date"]);
				$output->record($comment, "comment");
			}

			$output->close_tag();
		}
	}
?>
