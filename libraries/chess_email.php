<?php
	class chess_email extends email {
		private $db = null;
		protected $content_type = "text/html";
		private $link = "/";

		public function __construct($db, $subject) {
			$this->db = $db;

			parent::__construct($subject, "chess@leisink.net", "Online Chess website");
		}

		public function set_link($link, $user_id) {
			$this->link = $link;

			if (($key = one_time_key($this->db, $user_id)) !== false) {
				$this->link .= "?login=".$key;
			}
		}

		public function message($content) {	
			$content = str_replace("\n\n", "</p><p>", $content);
			$content = str_replace("\n", "<br>", $content);
			$content = str_replace("</p><p>", "</p>\n<p>", $content);

			$link = "Click <a href=\"http://".$_SERVER["SERVER_NAME"].$this->link."\">here</a> to visit the Online Chess website.";

			$message = file_get_contents("../extra/chess_email.txt");
			$message = str_replace("[TITLE]", $this->subject, $message);
			$message = str_replace("[CONTENT]", $content, $message);
			$message = str_replace("[LINK]", $link, $message);

			$this->message = $message;
		}
	}
?>
