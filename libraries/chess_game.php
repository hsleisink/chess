<?php
	class chess_game {
		protected $db = null;
		protected $error = false;

		protected $game_id = null;
		protected $my_game = false;
		protected $info = null;
		protected $white = null;
		protected $black = null;
		protected $player = null;
		protected $opponent = null;

		protected $moves = array();
		protected $move_count = 0;

		protected $my_color = null;
		protected $my_turn = false;
		protected $check = false;

		protected $default_board = array(
			array(WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN, WHITE_KING, WHITE_BISHOP, WHITE_KNIGHT, WHITE_ROOK),
			array(WHITE_PAWN, WHITE_PAWN,   WHITE_PAWN,   WHITE_PAWN,  WHITE_PAWN, WHITE_PAWN,   WHITE_PAWN,   WHITE_PAWN),
			array(NOTHING,    NOTHING,      NOTHING,      NOTHING,     NOTHING,    NOTHING,      NOTHING,      NOTHING),
			array(NOTHING,    NOTHING,      NOTHING,      NOTHING,     NOTHING,    NOTHING,      NOTHING,      NOTHING),
			array(NOTHING,    NOTHING,      NOTHING,      NOTHING,     NOTHING,    NOTHING,      NOTHING,      NOTHING),
			array(NOTHING,    NOTHING,      NOTHING,      NOTHING,     NOTHING,    NOTHING,      NOTHING,      NOTHING),
			array(BLACK_PAWN, BLACK_PAWN,   BLACK_PAWN,   BLACK_PAWN,  BLACK_PAWN, BLACK_PAWN,   BLACK_PAWN,   BLACK_PAWN),
			array(BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN, BLACK_KING, BLACK_BISHOP, BLACK_KNIGHT, BLACK_ROOK));
		protected $board;

		protected $white_king_moved = false;
		protected $black_king_moved = false;
		protected $left_white_rook_moved = false;
		protected $right_white_rook_moved = false;
		protected $left_black_rook_moved = false;
		protected $right_black_rook_moved = false;

		public function __construct($db, $game_id, $user_id = null) {
			$this->board = $this->default_board;
			$this->db = $db;
			$this->game_id = $game_id;

			if (($this->info = $db->entry("games", $game_id)) == false) {
				$this->error = true;
			} else {
				$this->white = $db->entry("users", $this->info["white"]);
				$this->black = $db->entry("users", $this->info["black"]);

				if ($user_id == $this->white["id"]) {
					$this->player = &$this->white;
					$this->opponent = &$this->black;
					$this->my_color = WHITE;
				} else if ($user_id == $this->black["id"]) {
					$this->player = &$this->black;
					$this->opponent = &$this->white;
					$this->my_color = BLACK;
				}

				$query = "select * from moves where game_id=%d order by id";
				$this->moves = $this->db->execute($query, $game_id);

				if (($this->my_color !== null) && ($this->info["surrender"] != $user_id) && ($this->info["draw"] != $user_id)) {
					$this->my_turn = $this->my_color == WHITE;
				}
			}
		}

		public function __clone() {
			$this->check = false;
		}

		public function __get($var) {
			if ($var == "error") {
				return $this->error;
			} else if ($var == "game_id") {
				return $this->game_id;
			} else if ($var == "board") {
				return $this->board;
			} else if ($var == "info") {
				return $this->info;
			} else if ($var == "white") {
				return $this->white;
			} else if ($var == "black") {
				return $this->black;
			} else if ($var == "player") {
				return $this->player;
			} else if ($var == "opponent") {
				return $this->opponent;
			} else if ($var == "my_turn") {
				return $this->my_turn;
			} else if ($var == "my_color") {
				return $this->my_color;
			} else if ($var == "draw_offer") {
				return $this->info["draw"] == $this->opponent["id"];
			} else if ($var == "surrendered") {
				return $this->info["surrender"] == $this->opponent["id"];
			} else if ($var == "check") {
				return $this->check;
			} else if ($var == "move_count") {
				return $this->move_count;
			}

			return null;
		}

		protected function position_to_coordinate($pos) {
			$x = ord($pos[0]) - ord("a");
			$y = (int)$pos[1] - 1;

			return array($x, $y);
		}

		protected function valid_position($pos) {
			if (strlen($pos) == 2) {
				if (($pos[0] >= "a") && ($pos[0] <= "h")) {
					if (($pos[1] >= "1") && ($pos[1] <= "8")) {
						return true;
					}
				}
			}

			return false;
		}

		protected function piece_color($piece) {
			if ($piece == NOTHING) {
				return -1;
			}

			return ($piece >= BLACK_PAWN ? BLACK : WHITE);
		}

		public function draw_board($output, $texture, $flip = false) {
			$output->add_javascript("banshee/ajax.js");
			if ($this->my_turn) {
				$output->add_javascript("game_play.js");
			} else {
				$output->add_javascript("game_wait.js");
			}

			$output->add_css("includes/chess.css");
			$output->add_css("includes/chessboard_".$texture.".css");

			$pieces = array(
				0  => "",
				1  => "white pawn",
				2  => "white rook",
				3  => "white knight",
				4  => "white bishop",
				5  => "white queen",
				6  => "white king",
				11 => "black pawn",
				12 => "black rook",
				13 => "black knight",
				14 => "black bishop",
				15 => "black queen",
				16 => "black king"
			);

			if ($flip) {
				$y_start = 0;
				$y_end   = 8;
				$y_delta = 1;

				$x_start = 7;
				$x_end   = -1;
				$x_delta = -1;
			} else {
				$y_start = 7;
				$y_end   = -1;
				$y_delta = -1;

				$x_start = 0;
				$x_end   = 8;
				$x_delta = 1;
			}

			$color = 0;
			$output->open_tag("chessboard", array("flipped" => show_boolean($flip), "texture" => $texture));
			for ($y = $y_start; $y != $y_end; $y += $y_delta) {
				$output->open_tag("row");
				for ($x = $x_start; $x != $x_end; $x += $x_delta) {
					$output->add_tag("cell", $this->board[$y][$x], array(
						"color" => $color == WHITE ? "white" : "black",
						"label" => chr(97 + $x).(string)($y + 1),
						"piece" => $pieces[$this->board[$y][$x]]));
					$color = 1 - $color;
				}
				$output->close_tag();
				$color = 1 - $color;
			}
			$output->close_tag();
		}

		public function show_moves($output, $count = null) {
			$names = array(
				0  => "",
				1  => "",
				2  => "R",
				3  => "N",
				4  => "B",
				5  => "Q",
				6  => "K",
				11 => "",
				12 => "R",
				13 => "N",
				14 => "B",
				15 => "Q",
				16 => "K");

			$color = 0;
			$number = 1;
			$max = count($this->moves);
			if ($count == null) {
				$count = $max;
			}

			$output->open_tag("moves", array(
				"count" => $count,
				"max" => $max));
			foreach ($this->moves as $move) {
				if ($count !== null) {
					if ($count-- == 0) {
						break;
					}
				}

				$params = array();
				$params["color"] = $color == WHITE ? "white" : "black";
				$params["date"] = $move["date"];
				if ($color == 0) {
					$params["number"] = ($number++).":";
				}
				$seperator = $move["hit"] ? " x " : " - ";
				$text = $names[$move["piece"]].$move["from"]." ".$seperator." ".$move["to"];
				if (($move["piece"] == WHITE_KING) || ($move["piece"] == BLACK_KING)) {
					if (($move["from"] == "e1") || ($move["from"] == "e8")) {
						if (($move["to"] == "c1") || ($move["to"] == "c8")) {
							$text = "0 - 0 - 0";
						} else if (($move["to"] == "g1") || ($move["to"] == "g8")) {
							$text = "0 - 0";
						}
					}
				}

				$output->add_tag("move", $text, $params);

				$color = 1 - $color;
			}
			$output->close_tag();
		}

		protected function path_clear($x1, $y1, $x2, $y2) {
			if (($div_x = $x2 - $x1) != 0) {
				$div_x = $div_x / abs($div_x);
			}
			if (($div_y = $y2 - $y1) != 0) {
				$div_y = $div_y / abs($div_y);
			}

			$max_loop = 8;

			$x1 += $div_x;
			$y1 += $div_y;
			while (($x1 != $x2) || ($y1 != $y2)) {
				if ($this->board[$y1][$x1] != NOTHING) {
					return false;
				}
				$x1 += $div_x;
				$y1 += $div_y;

				if (--$max_loop == 0) {
					return false;
				}
			}

			return true;
		}

		protected function make_move($from, $to, $promotion) {
			$promote_white = array(
				0 => WHITE_QUEEN,
				1 => WHITE_BISHOP,
				2 => WHITE_KNIGHT,
				3 => WHITE_ROOK);

			$promote_black = array(
				0 => BLACK_QUEEN,
				1 => BLACK_BISHOP,
				2 => BLACK_KNIGHT,
				3 => BLACK_ROOK);

			list($from_x, $from_y) = $this->position_to_coordinate($from);
			list($to_x, $to_y) = $this->position_to_coordinate($to);

			$piece = $this->board[$from_y][$from_x];
			$target = $this->board[$to_y][$to_x];

			/* Make the move
			 */
			$this->board[$to_y][$to_x] = $piece;
			$this->board[$from_y][$from_x] = NOTHING;

			if (($piece == WHITE_KING) || ($piece == BLACK_KING)) {
				/* Castling
				 */
				if (abs($to_x - $from_x) == 2) {
					if ($to_x == 2) {
						$this->board[$to_y][3] = $this->board[$to_y][0];
						$this->board[$to_y][0] = NOTHING;
					} else if ($to_x == 6) {
						$this->board[$to_y][5] = $this->board[$to_y][7];
						$this->board[$to_y][7] = NOTHING;
					}
				}
			} else if (($piece == WHITE_PAWN) && ($to_y == 7)) {
				/* White pawn reaches other side
				 */
				$this->board[$to_y][$to_x] = $promote_white[$promotion];
			} else if (($piece == BLACK_PAWN) && ($to_y == 0)) {
				/* Black pawn reaches other side
				 */
				$this->board[$to_y][$to_x] = $promote_black[$promotion];
			} else if (($target == NOTHING) && ($from_x != $to_x)) {
				/* En Passant
				 */
				if ($piece == WHITE_PAWN) {
					$this->board[$to_y - 1][$to_x] = NOTHING;
				} else if ($piece == BLACK_PAWN) {
					$this->board[$to_y + 1][$to_x] = NOTHING;
				}
			}

			$this->move_count++;
			if (($this->error == false) && ($this->my_color !== null)) {	
				$this->my_turn = !$this->my_turn;
			}

			/* Detect movement
			 */
			if ($from_y == 0) {
				if ($from_x == 0) {
					$this->left_white_rook_moved = true;
				} else if ($from_x == 4) {
					$this->white_king_moved = true;
				} else if ($from_x == 7) {
					$this->right_white_rook_moved = true;
				}
			} else if ($from_y == 7) {
				if ($from_x == 0) {
					$this->left_black_rook_moved = true;
				} else if ($from_x == 4) {
					$this->black_king_moved = true;
				} else if ($from_x == 7) {
					$this->right_black_rook_moved = true;
				}
			}
		}

		public function make_moves($steps = null) {
			if ($this->error) {
				return false;
			}

			$count = $this->move_count;

			foreach ($this->moves as $index => $move) {
				if ($count-- > 0) {
					continue;
				}

				if ($steps !== null) {
					if ($steps-- <= 0) {
						break;
					}
				}

				list($to_x, $to_y) = $this->position_to_coordinate($move["to"]);
				$this->moves[$index]["hit"] = $this->board[$to_y][$to_x] != NOTHING;
				$this->make_move($move["from"], $move["to"], $move["promotion"]);
				$this->moves[$index]["piece"] = $this->board[$to_y][$to_x];
			}

			$this->check = $this->king_under_attack();

			if (($this->info["draw"] == $this->opponent["id"]) || ($this->info["surrender"] == $this->opponent["id"])) {
				$this->my_turn = true;
			}

			return true;
		}

		public function new_move($from, $to, $promotion) {
			if ($this->my_turn == false) {	
				return false;
			}

			list($to_x, $to_y) = $this->position_to_coordinate($to);
			$hit = $this->board[$to_y][$to_x] != NOTHING;
			$this->make_move($from, $to, $promotion);
			$piece = $this->board[$to_y][$to_x];

			$this->check = $this->king_under_attack();

			$data = array(
				"id"        => null,
				"game_id"   => $this->game_id,
				"from"      => $from,
				"to"        => $to,
				"date"      => null,
				"promotion" => $promotion);
			$result = $this->db->insert("moves", $data);
			if ($result != false) {
				array_push($this->moves, array(
					"id"        => null,
					"game_id"   => $this->game_id,
					"from"      => $from,
					"to"        => $to,
					"date"      => time(),
					"promotion" => $promotion,
					"piece"     => $piece,
					"hit"       => $hit));
			}

			return $result;
		}

		public function valid_move($from, $to, $promotion) {
			/* Valid promotion?
			 */
			if (($promotion < 0) || ($promotion > 3)) {
				return false;
			}

			/* Invalid positions?
			 */
			if (($this->valid_position($from) == false) || ($this->valid_position($to) == false)) {
				return false;
			}

			/* Nothing moved?
			 */
			if ($from == $to) {
				return false;
			}

			list($from_x, $from_y) = $this->position_to_coordinate($from);
			list($to_x, $to_y) = $this->position_to_coordinate($to);
			$piece = $this->board[$from_y][$from_x];
			$target = $this->board[$to_y][$to_x];

			/* From-cell holds no piece
			 */
			if ($piece == NOTHING) {
				return false;
			}

			/* Moving own piece?
			 */
			if ($this->piece_color($piece) != $this->my_color) {
				return false;
			}

			/* Hit own piece?
			 */
			if ($target != NOTHING) {
				if ($this->piece_color($piece) == $this->piece_color($target)) {
					return false;
				}
			}

			/* En Passant pre-checks
			 */
			$white_en_passant = array_fill(0, 8, false);
			$black_en_passant = array_fill(0, 8, false);
			foreach ($this->moves as $move) {
				if (($move["piece"] != WHITE_PAWN) && ($move["piece"] != BLACK_PAWN)) {
					continue;
				}

				$white_en_passant = array_fill(0, 8, false);
				$black_en_passant = array_fill(0, 8, false);

				list($x_from, $y_from) = $this->position_to_coordinate($move["from"]);
				list($x_to, $y_to) = $this->position_to_coordinate($move["to"]);

				if ((abs($y_from - $y_to) == 2) && ($x_from == $x_to)) {
					if ($y_from == 1) {
						$white_en_passant[$x_from] = true;
					} else if ($y_from == 6) {
						$black_en_passant[$x_from] = true;
					}
				}
			}

			/* Check move
			 */
			switch ($piece) {
				case WHITE_PAWN:
					if ($from_y + 1 == $to_y) {
						if ($from_x == $to_x) {
							if ($target == NOTHING) {
								break;
							}
						} else if (abs($from_x - $to_x) == 1) {
							if ($target != NOTHING) {
								break;
							} else if (($from_y == 4) && ($this->board[$to_y - 1][$to_x] == BLACK_PAWN) && ($black_en_passant[$to_x])) {
								break;
							}
						}
					} else if (($from_y + 2 == $to_y) && ($from_y == 1)) {
						if ($from_x == $to_x) {
							if ($target == NOTHING) {
								break;
							}
						}
					}
					return false;
				case BLACK_PAWN:
					if ($from_y == $to_y + 1) {
						if ($from_x == $to_x) {
							if ($target == NOTHING) {
								break;
							}
						} else if (abs($from_x - $to_x) == 1) {
							if ($target != NOTHING) {
								break;
							} else if (($from_y == 3) && ($this->board[$to_y + 1][$to_x] == WHITE_PAWN) && ($white_en_passant[$to_x])) {
								break;
							}
						}
					} else if (($from_y == $to_y + 2) && ($from_y == 6)) {
						if ($from_x == $to_x) {
							if ($target == NOTHING) {
								break;
							}
						}
					}
					return false;
				case WHITE_ROOK:
				case BLACK_ROOK:
					if (($from_x == $to_x) || ($from_y == $to_y)) {
						if ($this->path_clear($from_x, $from_y, $to_x, $to_y)) {
							break;
						}
					}
					return false;
				case WHITE_KNIGHT:
				case BLACK_KNIGHT:
					if ((abs($from_x - $to_x) == 1) && (abs($from_y - $to_y) == 2)) {
						break;
					} else if ((abs($from_x - $to_x) == 2) && (abs($from_y - $to_y) == 1)) {
						break;
					}
					return false;
				case WHITE_BISHOP:
				case BLACK_BISHOP:
					if (abs($from_x - $to_x) == abs($from_y - $to_y)) {
						if ($this->path_clear($from_x, $from_y, $to_x, $to_y)) {
							break;
						}
					}
					return false;
				case WHITE_QUEEN:
				case BLACK_QUEEN:
					if (($from_x == $to_x) || ($from_y == $to_y)) {
						if ($this->path_clear($from_x, $from_y, $to_x, $to_y)) {
							break;
						}
					}
					if (abs($from_x - $to_x) == abs($from_y - $to_y)) {
						if ($this->path_clear($from_x, $from_y, $to_x, $to_y)) {
							break;
						}
					}
					return false;
				case WHITE_KING:
				case BLACK_KING;
					if (($from_x == $to_x) && (abs($from_y - $to_y) == 1)) {
						break;
					}
					if (($from_y == $to_y) && (abs($from_x - $to_x) == 1)) {
						break;
					}
					if ((abs($from_x - $to_x) == 1) && (abs($from_y - $to_y) == 1)) {
						break;
					}
					if ($this->check == false) {
						/* Castling?
						 */
						if ($piece == WHITE_KING) {
							$king_y = 0;
							$king_moved = $this->white_king_moved;
							$left_rook_moved = $this->left_white_rook_moved;
							$right_rook_moved = $this->right_white_rook_moved;
						} else {
							$king_y = 7;
							$king_moved = $this->black_king_moved;
							$left_rook_moved = $this->left_black_rook_moved;
							$right_rook_moved = $this->right_black_rook_moved;
						}
						if (($to_y == $king_y) && ($king_moved == false)) {
							if (($to_x == 2) && ($left_rook_moved == false)) {
								$clear = $this->path_clear($from_x, $from_y, 0, $to_y);
								$attack = $this->cell_under_attack(3, $from_y);
							} else if (($to_x == 6) && ($right_rook_moved == false)) {
								$clear = $this->path_clear($from_x, $from_y, 7, $to_y);
								$attack = $this->cell_under_attack(5, $from_y);
							}
							if ($clear && ($attack == false)) {
								break;
							}
						}
					}
					return false;
			}

			/* Check check
			 */
			$check_test = clone $this;
			$check_test->make_move($from, $to, PROMOTE_QUEEN);
			if ($check_test->king_under_attack()) {
				return false;
			}

			return true;
		}

		public function king_under_attack() {
			$king = $this->my_color == WHITE ? WHITE_KING : BLACK_KING;

			$king_found = false;
			for ($king_y = 0; $king_y < 8; $king_y++) {
				for ($king_x = 0; $king_x < 8; $king_x++) {
					if ($this->board[$king_y][$king_x] == $king) {
						$king_found = true;
						break 2;
					}
				}
			}

			if ($king_found == false) {
				return false;
			}

			return $this->cell_under_attack($king_x, $king_y);
		}

		public function cell_under_attack($cell_x, $cell_y) {
			/* Under attack by king or knight or pawn?
			 */
			$check_sets = array(
				array(
					"offsets" => array(
						array(-1,  1), array(0,  1), array(1,  1),
						array(-1,  0),               array(1,  0),
						array(-1, -1), array(0, -1), array(1, -1)),
					"piece" => $this->my_color == WHITE ? BLACK_KING : WHITE_KING),
				array(
					"offsets" => array(
						array( 1,  2), array( 2,  1),
						array(-1,  2), array(-2,  1),
						array( 1, -2), array( 2, -1),
						array(-1, -2), array(-2, -1)),
					"piece" => $this->my_color == WHITE ? BLACK_KNIGHT : WHITE_KNIGHT),
				array(
					"offsets" => $this->my_color == WHITE ? array(array(1, 1), array(-1, 1))
					                                      : array(array(-1, -1), array(1, -1)),
					"piece" => $this->my_color == WHITE ? BLACK_PAWN : WHITE_PAWN));

			foreach ($check_sets as $set) {
				foreach ($set["offsets"] as $offset) {
					$x = $cell_x + $offset[0];
					$y = $cell_y + $offset[1];
					if (($x >= 0) || ($x <= 7) || ($y >= 0) || ($y <= 7)) {
						if ($this->board[$y][$x] == $set["piece"]) {
							return true;
						}
					}
				}
			}

			/* Under attack by queen, rook or bishop?
			 */
			$check_sets = array(
				array(
					"offsets" => array(
						array(0, 1), array( 0, -1),
						array(1, 0), array(-1,  0)),
					"pieces" => $this->my_color == WHITE ? array(BLACK_ROOK, BLACK_QUEEN) : array(WHITE_ROOK, WHITE_QUEEN)),
				array(
					"offsets" => array(
						array( 1, 1), array( 1, -1),
						array(-1, 1), array(-1, -1)),
					"pieces" => $pieces = $this->my_color == WHITE ? array(BLACK_BISHOP, BLACK_QUEEN) : array(WHITE_BISHOP, WHITE_QUEEN)));

			foreach ($check_sets as $set) {
				foreach ($set["offsets"] as $offset) {
					$x = $cell_x + $offset[0];
					$y = $cell_y + $offset[1];
					while (($x >= 0) && ($x <= 7) && ($y >= 0) && ($y <= 7)) {
						if (in_array($this->board[$y][$x], $set["pieces"])) {
							return true;
						} else if ($this->board[$y][$x] != NOTHING) {
							break;
						}
						$x += $offset[0];
						$y += $offset[1];
					}
				}
			}

			return false;
		}

		public function surrender() {
			if ($this->my_turn == false) {
				return false;
			}

			$this->my_turn = false;
			return $this->db->update("games", $this->game_id, array("surrender" => $this->player["id"])) !== false;
		}

		public function offer_draw() {
			if ($this->my_turn == false) {
				return false;
			}

			$this->my_turn = false;
			return $this->db->update("games", $this->game_id, array("draw" => $this->player["id"])) !== false;
		}

		public function reject_draw_offer() {
			$query = "update games set draw=0 where id=%d and draw=%d";

			$this->my_turn = false;
			$this->info["draw"] = 0;
			return $this->db->query($query, $this->game_id, $this->opponent["id"]) !== false;
		}

		public function end_game() {
			if ($this->surrendered == false) {
				if ($this->my_turn == false) {
					return false;
				} else {
					$this->info["status"] = GAME_DONE;
				}
			}

			return $this->db->update("games", $this->game_id, array("status" => GAME_DONE)) !== false;
		}
	}
?>
