<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="includes/chess.xslt" />

<!--
//
//  Games template
//
//-->
<xsl:template match="games">
<h1>Play chess game</h1>

<div class="row">
<div class="col-sm-9">
<xsl:choose>
<xsl:when test="game">
<table class="table table-striped">
<thead>
<tr><th>Opponent</th><th>Start date</th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="game">
	<tr>
	<td><a href="/game/{@id}"><xsl:value-of select="opponent" /></a></td>
	<td><xsl:value-of select="start" /></td>
	<td><xsl:if test="my_turn='yes'">It's your turn!</xsl:if></td>
	</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:when>
<xsl:otherwise>
<p>You are not playing any game. Go <a href="/challenge">challenge</a> someone first.</p>
</xsl:otherwise>
</xsl:choose>
</div>
<div class="col-sm-3">
<img src="/images/king{@theme}.jpg" class="king" />
</div>
</div>
</xsl:template>

<!--
//
//  Game template
//
//-->
<xsl:template match="game">
<script type="text/javascript">
	var game_id = <xsl:value-of select="@id" />;
</script>

<div class="row">
<div class="col-sm-8">
<xsl:choose>
<xsl:when test="@draw_offer='yes' or @surrendered='yes'">
	<xsl:apply-templates select="chessboard" />
</xsl:when>
<xsl:when test="@my_turn='yes'">
	<script type="text/javascript">
		var board_flipped = '<xsl:value-of select="chessboard/@flipped" />';
		var my_color = <xsl:value-of select="my_color" />;
	</script>
	<table class="chessboard" cellspacing="0">
	<xsl:for-each select="chessboard/row">
		<tr>
		<xsl:for-each select="cell">
			<td class="click {@color}" onMouseOver="javascript:cell_hover('{@label}')"
				onMouseOut="javascript:cell_leave()" onClick="javascript:cell_clicked('{@label}')">
			<input type="hidden" id="cell_{@label}" value="{.}" />
			<xsl:if test=".!=0">
				<img src="/images/pieces/{.}.png" />
			</xsl:if>
			</td>
		</xsl:for-each>
		</tr>
	</xsl:for-each>
	</table>
	<div class="marker" id="marker1" />
	<div class="marker" id="marker2" />
</xsl:when>
<xsl:otherwise>
	<script type="text/javascript">
		var my_color = <xsl:value-of select="my_color" />;
	</script>
	<xsl:apply-templates select="chessboard" />
</xsl:otherwise>
</xsl:choose>
</div>

<div class="col-sm-4 controls">
<h3>Players:</h3>
<div>White: <xsl:value-of select="white" /></div>
<div>Black: <xsl:value-of select="black" /></div>

<h3>Move history:</h3>
<xsl:apply-templates select="moves" />

<h3>Current move:</h3>
<xsl:choose>
<xsl:when test="@surrendered='yes'">
<p>Your opponent has surrendered.</p>
</xsl:when>
<xsl:when test="@draw_offer='yes'">
<form action="/game/{@id}" method="post">
<div>Your opponent has offered you a draw.</div>
<div class="btn-group">
<input type="submit" name="submit_button" value="Accept" class="btn btn-default btn-sm" />
<input type="submit" name="submit_button" value="Reject" class="btn btn-default btn-sm" />
</div>
</form>
</xsl:when>
<xsl:when test="@my_turn='yes'">
<form action="/game/{@id}" method="post">
<p>From: <input type="text" name="from" id="from" value="" readonly="readonly" class="move" />
   To: <input type="text" name="to" id="to" value="" readonly="readonly" class="move" /></p>
<div class="btn-group btn-cmds">
<input type="submit" name="submit_button" value="Move" class="btn btn-primary btn-sm" />
<input type="button" value="Clear" onClick="javascript:move_reset()" class="btn btn-default btn-sm" />
</div>
<div class="btn-group btn-cmds">
<input type="submit" name="submit_button" value="Resign" onClick="javascript:return confirm('Resign: Are you sure?')" class="btn btn-default btn-sm" />
<input type="submit" name="submit_button" value="Offer draw" onClick="javascript:return confirm('OFFER DRAW: Are you sure?')" class="btn btn-default btn-sm" />
</div>
<div class="btn-group btn-cmds">
<a href="/game/{@id}" class="btn btn-default btn-sm">Refresh</a>
<a href="/game" class="btn btn-default btn-sm">Back</a>
</div>


<div class="promotion" id="promotion">
Promotion: <select name="promotion" class="promotion text">
<option value="0">Queen</option>
<option value="1">Bishop</option>
<option value="2">Knight</option>
<option value="3">Rook</option>
</select>
</div>
</form>
</xsl:when>
<xsl:otherwise>
	<p>It's not your turn.</p>
	<a href="/game/{@id}" class="btn btn-default btn-sm">Refresh</a>
</xsl:otherwise>
</xsl:choose>

<xsl:call-template name="show_messages" />
<xsl:if test="@check='yes'">
<div class="alert alert-warning">CHECK!</div>
</xsl:if>

</div>
</div>

<script type="text/javascript" src="/js/chess.js" />

<div class="comments" id="comments">
<h2>Comments</h2>
<xsl:for-each select="comments/comment">
<div><b><xsl:value-of select="author" />, <xsl:value-of select="date" /></b>: <xsl:value-of select="comment" /></div>
</xsl:for-each>

<form action="/game/{@id}" method="post">
<textarea name="comment" class="form-control"></textarea>
<div class="btn-group">
<input type="submit" name="submit_button" value="Add comment" class="btn btn-default" />
</div>
</form>
</div>
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<h1>Chess games</h1>
<p><xsl:value-of select="." /></p>
<xsl:call-template name="redirect" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="games" />
<xsl:apply-templates select="game" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
