<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Game template
//
//-->
<xsl:template match="game">
<div class="players">
<b>Game: </b> <xsl:value-of select="@id" />, <b>White: </b> <xsl:value-of select="white" />, <b>Black: </b><xsl:value-of select="black" />
</div>

<div class="moves">
<xsl:for-each select="moves/round">
<xsl:if test="move">
<div class="round">
<div class="number"><xsl:value-of select="position()" />:</div>
<xsl:for-each select="move">
<div class="move"><xsl:value-of select="from" /> - <xsl:value-of select="to" /></div>
</xsl:for-each>
</div>
</xsl:if>
</xsl:for-each>
</div>

<br style="clear:both" />

<div class="btn-group">
<input type="button" value="Back" onClick="javascript:document.location='/view/{@id}'" class="btn btn-default" />
</div>

<script type="text/javascript">
window.print();
</script>
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<p><xsl:value-of select="." /></p>
<xsl:choose>
	<xsl:when test="@url">
		<xsl:call-template name="redirect"><xsl:with-param name="url" select="@url" /></xsl:call-template>
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="redirect" />
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Moves</h1>
<xsl:apply-templates select="game" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
