<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">

<xsl:template match="chessboard">
<table class="chessboard" cellspacing="0">
<xsl:for-each select="row">
	<tr>
	<xsl:for-each select="cell">
		<td class="{@color}">
			<xsl:if test=".!=0">
			<img src="/images/pieces/{.}.png" alt="{@piece}" />
			</xsl:if>
		</td>
	</xsl:for-each>
	</tr>
</xsl:for-each>
</table>
</xsl:template>

<xsl:template match="moves">
<div class="moves" id="history">
<table>
<xsl:for-each select="move">
	<tr>
	<td class="number"><xsl:value-of select="@number" /></td>
	<td class="{@color}"><xsl:value-of select="." /></td>
	</tr>
</xsl:for-each>
</table>
</div>
</xsl:template>

</xsl:stylesheet>
