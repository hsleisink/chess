<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="banshee/main.xslt" />

<xsl:template match="content">
<h1>Statistics</h1>
<table class="table table-striped table-condensed" id="stats">
<thead>
	<tr><th>Name</th><th sort="integer">Won</th><th sort="integer">Lost</th><th sort="integer">Draw</th><th sort="integer">Active</th><th sort="integer">Total</th><th sort="integer">Win rate</th><th>Score</th></tr>
</thead>
<tbody>
<xsl:for-each select="statistics/user">
	<tr>
	<td class="name"><xsl:value-of select="fullname" /></td>
	<td class="stat"><xsl:value-of select="games_won" /></td>
	<td class="stat"><xsl:value-of select="games_lost" /></td>
	<td class="stat"><xsl:value-of select="games_draw" /></td>
	<td class="stat"><xsl:value-of select="games_active" /></td>
	<td class="stat"><xsl:value-of select="games_total" /></td>
	<td class="stat"><xsl:value-of select="rate" />%</td>
	<td class="stat"><xsl:value-of select="score" /></td>
	</tr>
</xsl:for-each>
</tbody>
</table>
</xsl:template>

</xsl:stylesheet>
