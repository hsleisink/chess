<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="banshee/main.xslt" />

<!--
//
//  Overview template
//
//-->
<xsl:template match="overview">
<script type="text/javascript" src="/js/ajax.js" />
<script type="text/javascript" src="/js/challenge.js" />
<h2>Select a user to challenge</h2>
<xsl:choose>
<xsl:when test="count(users/user)=0">
	<p>No users left to challenge. Maybe finish your current games first?</p>
</xsl:when>
<xsl:otherwise>
	<form action="/challenge" method="post">
	<table class="table table-striped table-condensed">
	<thead>
	<tr><th></th><th class="name">Name</th><th class="color">Last color*</th></tr>
	</thead>
	<tbody>
	<xsl:for-each select="users/user">
	<tr>
	<td><input type="radio" name="opponent_id" value="{@id}" class="user" /></td>
	<td><xsl:value-of select="fullname" /></td>
	<td><xsl:value-of select="last_color" /></td>
	<td><xsl:if test="online!='no'">user online (<xsl:value-of select="online" /> ago)</xsl:if></td>
	</tr>
	</xsl:for-each>
	</tbody>
	</table>

	Challenger plays with <input type="radio" name="my_color" value="0" class="color" /> white or <input type="radio" name="my_color" value="1" class="color" checked="checked" /> black.
	<input type="submit" name="submit_button" value="Challenge" class="btn btn-primary btn-xs" />
	<div class="last_color">Last color: your color in the previous match against this opponent.</div>
	<xsl:call-template name="show_messages" />
	</form>
</xsl:otherwise>
</xsl:choose>

<h2>Pending games</h2>
<xsl:choose>
<xsl:when test="count(pending/game)=0">
	<p>No pending games.</p>
</xsl:when>
<xsl:otherwise>
	<xsl:for-each select="pending/game">
		<xsl:choose>
		<xsl:when test="creator=/output/user/@id">
			<div class="row">
			<div class="col-xs-7"><xsl:value-of select="opponent" /></div>
			<div class="col-xs-5">(still waiting for response)</div>
			</div>
		</xsl:when>
		<xsl:otherwise>
			<form action="/challenge" method="post">
			<input type="hidden" name="game_id" value="{@id}" />
			<div class="row">
			<div class="col-xs-7"><xsl:value-of select="opponent" /></div>
			<div class="col-xs-5">
			<div class="btn-group">
			<input type="submit" name="submit_button" value="Accept" class="btn btn-primary btn-xs" />
			<input type="submit" name="submit_button" value="Reject" class="btn btn-primary btn-xs" />
			</div>
			</div>
			</div>
			</form>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<p><xsl:value-of select="." /></p>
<xsl:call-template name="redirect">
	<xsl:with-param name="url"><xsl:value-of select="@url" /></xsl:with-param>
</xsl:call-template>
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Challenge user</h1>
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
