<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="includes/layout_site.xslt" />

<xsl:template match="content">
<h1>Instructions</h1>
<h2>Challenging another user</h2>
<p>To start a game of chess, challenge another user via the 'challenge user' menu option. An e-mail will be send to the user to inform him or her about your challenge. You will recieve an e-mail when that user has accepted or declined your challenge.</p>

<h2>Playing the game</h2>
<p>When it's your turn to make a move, simply click on the chess piece you want to move and then click on the desired destination. When you are sure about your move, press the 'move' button. Castling is done by moving the king two squares.</p>

<h2>E-mail notifications</h2>
<p>If you want to be notified via e-mail when your opponent has made a move, go to your profile, select 'Notify on move' and press the 'Update profile' button.</p>

<h2>Questions</h2>
<p>If you have any question, don't hesitate to <a href="mailto:Hugo Leisink &lt;hugo@leisink.net&gt;?subject=Question about the chess website">send me an e-mail</a>.</p>

<br /><br />

<span>
<input type="button" value="Main menu" onClick="javascript:document.location='/menu'" class="button" />
</span>
</xsl:template>

</xsl:stylesheet>
