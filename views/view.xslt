<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="banshee/main.xslt" />
<xsl:include href="includes/chess.xslt" />
<xsl:include href="banshee/pagination.xslt" />

<!--
//
//  Games template
//
//-->
<xsl:template match="overview">
<h1>View chess game</h1>
<h2><xsl:value-of select="games/@section" /> games</h2>
<table class="table table-striped table-condensed">
<thead>
<tr><th>Players</th><th><xsl:value-of select="games/@order" /></th><th></th></tr>
</thead>
<tbody>
<xsl:for-each select="games/game">
<tr>
	<td class="players"><a href="/view/{@id}"><xsl:value-of select="white" /> &#8211; <xsl:value-of select="black" /></a></td>
	<td class="start"><xsl:value-of select="start" /></td>
	<td class="comments"><xsl:if test="comments>0"><img src="/images/comments.gif" alt="comments" /></xsl:if></td>
</tr>
</xsl:for-each>
</tbody>
</table>
<xsl:apply-templates select="pagination" />

<xsl:choose>
	<xsl:when test="games/@section='Active'">
		<p>View all <a href="/view/finished">finished</a> or only <a href="/view/finished/my">my finished</a> games.</p>
	</xsl:when>
	<xsl:otherwise>
		<p>View the <a href="/view">active</a> games.</p>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>

<!--
//
//  Game template
//
//-->
<xsl:template match="game">
<div class="row">
<div class="col-sm-8">
<xsl:apply-templates select="chessboard" />
</div>

<div class="col-sm-4 controls">
<h3>Players:</h3>
<div>White: <xsl:value-of select="white" /></div>
<div>Black: <xsl:value-of select="black" /></div>

<xsl:if test="surrendered">
<div><xsl:value-of select="surrendered" /> surrendered.</div>
</xsl:if>
<xsl:if test="draw='yes'">
<div>Game ended in a draw.</div>
</xsl:if>

<h3>Move history:</h3>
<xsl:apply-templates select="moves" />

<h3>Browse buttons:</h3>
<div class="btn-group btn-cmds">
<input type="button" value="First" onClick="javascript:document.location='/view/{@id}/0'" class="btn btn-default btn-sm" />
<input type="button" value="Last" onClick="javascript:document.location='/view/{@id}/{moves/@max}'" class="btn btn-default btn-sm" />
</div>
<div class="btn-group btn-cmds">
<input type="button" value="&lt;&lt;" class="btn btn-default btn-sm">
	<xsl:choose>
	<xsl:when test="moves/@count &gt; 0">
			<xsl:attribute name="onClick">javascript:document.location='/view/<xsl:value-of select="@id" />/<xsl:value-of select="moves/@count - 1" />'</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
		<xsl:attribute name="disabled">disabled</xsl:attribute>
	</xsl:otherwise>
	</xsl:choose>
</input>
<input type="button" value="&gt;&gt;" class="btn btn-default btn-sm">
	<xsl:choose>
	<xsl:when test="moves/@count &lt; moves/@max">
			<xsl:attribute name="onClick">javascript:document.location='/view/<xsl:value-of select="@id" />/<xsl:value-of select="moves/@count + 1" />'</xsl:attribute>
	</xsl:when>
	<xsl:otherwise>
		<xsl:attribute name="disabled">disabled</xsl:attribute>
	</xsl:otherwise>
	</xsl:choose>
</input>
</div>
<div class="btn-group btn-cmds">
<input type="button" value="Back" onClick="javascript:document.location='/view{@link}'" class="btn btn-default btn-sm" />
<input type="button" value="Print moves" onClick="javascript:document.location='/moves/{@id}'" class="btn btn-default btn-sm" />
</div>

</div>
</div>

<xsl:if test="(comments/comment) or (@status=2)">
<div class="comments" id="comments">
<h2>Comments</h2>
<xsl:for-each select="comments/comment">
<div><b><xsl:value-of select="author" />, <xsl:value-of select="date" /></b>: <xsl:value-of select="comment" /></div>
</xsl:for-each>

<xsl:if test="@status=2">
<form action="/view/{@id}/{@moves}" method="post">
<textarea name="comment" class="form-control"></textarea>
<div class="btn-group">
<input type="submit" name="submit_button" value="Add comment" class="btn btn-default" />
</div>
</form>
</xsl:if>
</div>
</xsl:if>

<script type="text/javascript" src="/js/chess.js" />
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<h1>View chess game</h1>
<p><xsl:value-of select="." /></p>
<xsl:call-template name="redirect" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="overview" />
<xsl:apply-templates select="game" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
