<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:include href="../banshee/main.xslt" />

<!--
//
//  Games template
//
//-->
<xsl:template match="games">
<xsl:call-template name="show_messages" />
<table class="table table-striped">
<thead>
<tr><th>Players</th><th>Turn</th><th>Start date</th><th>Roll back</th></tr>
</thead>
<tbody>
<xsl:for-each select="game">
<tr>
<td><xsl:value-of select="white" /> - <xsl:value-of select="black" /></td>
<td><xsl:value-of select="turn" /></td>
<td><xsl:value-of select="start" /></td>
<td><form action="/{/output/page}" method="post"><input type="hidden" name="game_id" value="{@id}" /><input type="submit" name="submit_button" value="Roll back" class="btn btn-primary btn-xs" onClick="javascript:return confirm('ROLL BACK: Are you sure?')" /></form></td>
</tr>
</xsl:for-each>
</tbody>
</table>

<div class="btn-group">
<a href="/cms" class="btn btn-default">Back</a>
</div>
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<p><xsl:value-of select="." /></p>
<xsl:call-template name="redirect" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<h1>Rollback move</h1>
<xsl:apply-templates select="games" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
