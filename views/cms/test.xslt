<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:include href="../banshee/main.xslt" />
<xsl:include href="../includes/chess.xslt" />

<!--
//
//  Game template
//
//-->
<xsl:template match="game">
<div class="row">
<div class="col-md-8">
<xsl:choose>
<xsl:when test="@draw_offer='yes' or @surrendered='yes'">
	<xsl:apply-templates select="chessboard" />
</xsl:when>
<xsl:when test="@my_turn='yes'">
	<script type="text/javascript">
		var board_flipped = '<xsl:value-of select="chessboard/@flipped" />';
		var my_color = <xsl:value-of select="my_color" />;
	</script>
	<table class="chessboard" cellspacing="0">
	<xsl:for-each select="chessboard/row">
		<tr><xsl:text>
</xsl:text>
		<xsl:for-each select="cell">
			<td class="click {@color}"
			                     onMouseOver="javascript:cell_hover('{@label}')" 
			                     onMouseOut="javascript:cell_leave()"
			                     onClick="javascript:cell_clicked('{@label}')">
			<input type="hidden" id="cell_{@label}" value="{.}" />
			<xsl:if test=".!=0">
				<img src="/images/pieces/{.}.png" />
			</xsl:if>
			</td><xsl:text>
</xsl:text>
		</xsl:for-each>
		</tr><xsl:text>
</xsl:text>
	</xsl:for-each>
	</table>
	<div class="marker" id="marker1" />
	<div class="marker" id="marker2" />
</xsl:when>
<xsl:otherwise>
	<script type="text/javascript">
		var my_color = <xsl:value-of select="my_color" />;
	</script>
	<script type="text/javascript" src="/js/game_wait.js" />
	<xsl:apply-templates select="chessboard" />
</xsl:otherwise>
</xsl:choose>
</div>

<div class="col-md-4 controls">
<h3>Players:</h3>
<div>White: <xsl:value-of select="white" /></div>
<div>Black: <xsl:value-of select="black" /></div>

<h3>Move history:</h3>
<xsl:apply-templates select="moves" />

<h3>Current move:</h3>
<xsl:choose>
<xsl:when test="@my_turn='yes'">
	<form action="/{/output/page}" method="post">
	<p>From: <input type="text" name="from" id="from" value="" readonly="readonly" class="move" />
	   To: <input type="text" name="to" id="to" value="" readonly="readonly" class="move" /></p>

	<div class="btn-group btn-cmds">
	<input type="submit" name="submit_button" value="Move" class="btn btn-default btn-sm" />
	<input type="button" value="Clear" onClick="javascript:move_reset()" class="btn btn-default btn-sm" />
	</div>
	<div class="btn-group btn-cmds">
	<input type="submit" name="submit_button" value="Rollback" class="btn btn-default btn-sm" />
	<input type="submit" name="submit_button" value="Reset game" class="btn btn-default btn-sm" onClick="javascript:return confirm('RESET: Are you sure?')" />
	</div>

	<div class="promotion" id="promotion">
	Promotion: <select name="promotion" class="promotion text">
	<option value="0">Queen</option>
	<option value="1">Bishop</option>
	<option value="2">Knight</option>
	<option value="3">Rook</option>
	</select>
	</div>
	</form>
</xsl:when>
<xsl:otherwise>
	<p>It's not your turn.</p>
</xsl:otherwise>
</xsl:choose>

<xsl:call-template name="show_messages" />
<xsl:if test="@check='yes'">
<div class="alert alert-danger">CHECK!</div>
</xsl:if>
</div>
</div>

<script type="text/javascript" src="/js/chess.js" />
</xsl:template>

<!--
//
//  Result template
//
//-->
<xsl:template match="result">
<h1>Play chess game</h1>
<p><xsl:value-of select="." /></p>
<xsl:call-template name="redirect" />
</xsl:template>

<!--
//
//  Content template
//
//-->
<xsl:template match="content">
<xsl:apply-templates select="game" />
<xsl:apply-templates select="result" />
</xsl:template>

</xsl:stylesheet>
